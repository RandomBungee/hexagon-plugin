package com.gitlab.leonklein;

import com.gitlab.leonklein.client.Client;
import net.md_5.bungee.api.plugin.Plugin;

public class HexagonPlugin extends Plugin {
  public HexagonPlugin() {}

  public static final String PREFIX = "§8| §c§lGuardian §8» §r";
  public static final Client CLIENT = new Client();

  @Override
  public void onEnable() {
    System.out.println("Hexagon-Plugin [Guardian] loaded!");
  }
}
