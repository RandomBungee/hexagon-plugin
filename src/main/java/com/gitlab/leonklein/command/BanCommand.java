package com.gitlab.leonklein.command;

import com.gitlab.leonklein.HexagonPlugin;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BanCommand extends Command {

  public BanCommand(String name) {
    super(name);
  }

  @Override
  public void execute(CommandSender commandSender, String[] arguments) {
    if(checkCommandComponent(arguments, commandSender)) {
      return;
    }
    ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
    String targetPlayer = arguments[0];
    String banId = arguments[1];

  }

  private boolean checkCommandComponent(
      String[] arguments,
      CommandSender commandSender
  ) {
    if(!(commandSender instanceof ProxiedPlayer)) {
      commandSender.sendMessage(new TextComponent("§cDu musst ein Spieler sein!"));
      return true;
    }
    if(arguments.length != 2) {
      commandSender.sendMessage(new TextComponent(HexagonPlugin.PREFIX + "§cBitte nutze /ban <Player> <ID>"));
      return true;
    }
    if(!commandSender.hasPermission("guardian.ban")) {
      commandSender.sendMessage(new TextComponent(HexagonPlugin.PREFIX + "§7Dafür hast du §ckeine §7Rechte!"));
      return true;
    }
    return false;
  }

  private void banPlayerForId(
      String targetPlayer,
      String bannedBy,
      String banId
  ) {
    switch(banId) {
      case "1":
        HexagonPlugin.CLIENT.punishRepository().create(targetPlayer, "", "Hacking", bannedBy, calculateBanTime(banId));
        break;
    }
  }

  private long calculateBanTime(String banId) {
    long calculatedBanTime = (long) (getDefaultBanTime(banId) * 50 / 0.5);
    return calculatedBanTime;
  }

  private long getDefaultBanTime(String banId) {
    long banTime = 0;
    switch(banId) {
      case "1":
        banTime = 60 * 60 * 24 * 30;
        break;
    }
    return banTime;
  }
}
